# README #

### What is this repository for? ###

This is Kert's code for The Complete Guide to Angular.

## Boilerplate Usage

Follow the following steps and you're good to go! Important: Typescript and npm has to be installed on your machine!

1: Clone repo
```
git clone https://github.com/mschwarzmueller/angular-2-beta-boilerplate.git
```
2: Install packages
```
npm install
```
3: Start server (includes auto refreshing) and gulp watcher
```
npm start
```